"""
module docstring
"""
import subprocess
import os
import time

class Ffwrapper:
    """class docstring."""
    def __init__(self):
        # validar que no haya codec de audio OPUS D=
        self.input_file = None
        self.valid_file = False

    def validate_input_file(self, input_file):
        """Validate input method."""
        pass

    def h264_aac_encode(self, infile, out_opt, out_file=None):
        """H264 and AAC encode method.
            out_options puede ser un diccionario de la forma:
            {"crf": "22",
             "preset": "slow",
             "audio": [False, "copy", "96k"]
             "cut": True,
             "start": "32",
             "duration": 10,
             "scale": True,
             "height": "640"} """
        if os.path.exists(infile):
            infile_path = os.path.dirname(infile)
            infile_name = os.path.splitext(os.path.basename(infile))[0]
        else:
            print("Error: File path does not exist.")
            return False

        options = ["ffmpeg", "-y"]
        # -i
        options.append("-i")
        options.append(infile)
        # -c:v
        options.append("-c:v")
        options.append("libx264")
        # -crf
        options.append("-crf")
        if out_opt["crf"]:
            options.append(out_opt["crf"])
        else:
            options.append("25")
        # -preset
        options.append("-preset")
        if out_opt["preset"]:
            options.append(out_opt["preset"])
        else:
            options.append("slow")
        # -codec:audio
        if not out_opt["audio"]:
            options.append("-an")
        elif out_opt["audio"] == "copy":
            options.append("-c:a")
            options.append("copy")
        else:
            options.extend(["-c:a", "aac", "-strict", "-2", "-b:a", out_opt["audio"]])
        # -ss -t
        if out_opt["cut"]:
            options.extend(["-ss", out_opt["start"], "-t", out_opt["duration"]])
        # -vf scale
        if out_opt["scale"]:
            options.extend(["-vf", "scale=-1:{}".format(out_opt["height"])])
        # -out
        if out_file is None:
            out_file = infile_path + "/" + infile_name + "-" + str(time.strftime("%H%M%S")) + ".mp4"
        options.append(out_file)
        print("options:", options, sep=' ')
        completed_process = subprocess.run(options, stderr=subprocess.PIPE)
        #print(completed_process)
        print("return code: " + str(completed_process.returncode))
        print("*****h264_aac_encode has ended******")
        return completed_process.returncode
